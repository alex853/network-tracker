package net.simforge.networkview.world.airports;

public enum BoundaryType {
    Default,
    Circles,
    Polygon
}
