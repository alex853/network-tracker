package net.simforge.networkview.world.atmosphere;

public enum AltimeterMode {
    STD, QNH, QFE
}
